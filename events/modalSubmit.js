const { Events } = require('discord.js');
const sanitize = require('sanitize');
const sanitizer = new sanitize();
const sequelize = require('../db/database.js');
// eslint-disable-next-line no-unused-vars
const Game = require('../db/models/game.js');
const buildGameEmbed = require('../embeds/game-embed.js');

module.exports = {
	name: Events.InteractionCreate,
	async execute(interaction) {
		if (!interaction.isModalSubmit()) return;

		if (interaction.customId.includes('game')) {
			const userId = interaction.user.id;
			const name = sanitizer.value(interaction.fields.getTextInputValue('name'), 'str');
			const description = sanitizer.value(interaction.fields.getTextInputValue('description'), 'str');
			const protocol = interaction.fields.getTextInputValue('url').includes('https') ? 'https' : 'http';
			const url = sanitizer.value([interaction.fields.getTextInputValue('url'), protocol], 'url');
			const thumbnail = sanitizer.value([interaction.fields.getTextInputValue('thumbnail'), 'https'], 'url');
			const startDate = sanitizer.value(interaction.fields.getTextInputValue('startDate'), 'str');

			try {
				await sequelize.sync();
				const game = {
					user_id: userId,
					name: name,
					description: description,
					url: url,
					thumbnail_url: thumbnail,
					start_date: startDate,
				};

				if (interaction.customId.includes('add')) {
					const existing = await Game.findOne({
						where: sequelize.where(sequelize.fn('lower', sequelize.col('name')), sequelize.fn('lower', name)),
					});
					if (existing) {
						await interaction.reply({ content: 'game already exists in the database, did you mean to use /game-update?', ephemeral: true });
						return;
					}
					await sequelize.models.Game.create(game);
				}
				else if (interaction.customId.includes('edit')) {
					const existing = await Game.findOne({
						where: sequelize.where(sequelize.fn('lower', sequelize.col('name')), sequelize.fn('lower', name)),
					});
					if (existing && existing.user_id !== interaction.user.id) {
						await interaction.reply({ content: `${name} is already registered by a different user`, ephemeral: true });
						return;
					}
					await sequelize.models.Game.update(game, { where: { id: existing.id } });
				}
				const embed = buildGameEmbed(interaction.user.username, interaction.user.avatarURL(), name, description, url, thumbnail, startDate);
				interaction.reply({ embeds: [embed] });
			}
			catch (error) {
				console.error(error);
				interaction.reply({ content: 'something went wrong!!', ephemeral: true });
			}
		}
		else {
			interaction.reply('unhandled modal submit error');
		}
	},
};