const setup = require('./setup.js');
const path = require('path');
const { Client, GatewayIntentBits } = require('discord.js');

require('dotenv').config();

global.assetPath = path.resolve(__dirname + '/assets/');

const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.GuildMessageReactions] });
setup.command(client);
setup.events(client);
setup.database();

client.login(process.env.DISCORD_TOKENT);