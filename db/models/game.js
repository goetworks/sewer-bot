const { DataTypes } = require('sequelize');
const sequelize = require('../database.js');

const Game = sequelize.define('Game', {
	user_id: {
		type: DataTypes.TEXT,
		allowNull: false,
	},
	name: {
		type: DataTypes.TEXT,
		allowNull: false,
		unique: true,
	},
	description: {
		type: DataTypes.TEXT,
		allowNull: false,
	},
	url: {
		type: DataTypes.TEXT,
	},
	thumbnail_url: {
		type: DataTypes.TEXT,
	},
	start_date: {
		type: DataTypes.TEXT,
	},
});

module.exports = Game;