const { DataTypes } = require('sequelize');
const sequelize = require('../database.js');

const Demo = sequelize.define('Demo', {
	url: {
		type:DataTypes.TEXT,
		allowNull: false,
	},
	game_id: {
		type: DataTypes.INTEGER,
		allowNull: false,
	},
	release_date: {
		type: DataTypes.TEXT,
	},
});

module.exports = Demo;