const { SlashCommandBuilder, EmbedBuilder, AttachmentBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('poll')
		.setDescription('create a poll')
		.addStringOption(option =>
			option.setName('question')
				.setDescription('the prompt for the poll')
				.setRequired(true))
		.addStringOption(option =>
			option.setName('choice0')
				.setDescription('the first choice')
				.setRequired(true))
		.addStringOption(option =>
			option.setName('choice1')
				.setDescription('the correct choice')
				.setRequired(true))
		.addStringOption(option =>
			option.setName('choice2')
				.setDescription('a choice'))
		.addStringOption(option =>
			option.setName('choice3')
				.setDescription('a choice'))
		.addStringOption(option =>
			option.setName('choice4')
				.setDescription('a choice'))
		.addStringOption(option =>
			option.setName('choice5')
				.setDescription('a choice')),
	async execute(interaction) {
		const emojis = ['🅰', '🅱', '3️⃣', '4️⃣', '5️⃣', '6️⃣'];
		const question = interaction.options.getString('question');

		let description = '';
		const choices = [];
		for (let i = 0; i < 6; i++) {
			const choice = interaction.options.getString(`choice${i}`);
			if (choice != null) {
				description += `${emojis[choices.length]} ${choice} \n\n`;
				choices.push(choice);
			}
		}

		const fileCount = 9;
		const keys = [];
		const thumbnails = new Map();
		for (let index = 0; index < fileCount; index++) {
			keys.push(`attachment://thinking${index + 1}.jpg`);
			thumbnails.set(keys[index], new AttachmentBuilder(`assets/poll-pics/thinking${index + 1}.jpg`));
		}
		const roll = Math.floor(Math.random() * fileCount);
		const embed = new EmbedBuilder()
			.setTitle(question)
			.setDescription(description)
			.setThumbnail(keys[roll]);

		const message = await interaction.reply({ embeds: [embed], files: [thumbnails.get(keys[roll])], fetchReply: true });
		for (let index = 0; index < choices.length; index++) {
			const emoji = emojis[index];
			await message.react(emoji);
		}
	},
};