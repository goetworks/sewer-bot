const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('roll')
		.setDescription('random number for you fortune telling pleasure')
		.addNumberOption(input =>
			input.setName('max')
				.setDescription('upper bound (inclusive (sometimes not))'))
		.addNumberOption(input =>
			input.setName('min')
				.setDescription('lower bound (inclusive)')),
	async execute(interaction) {
		const a = interaction.options.getNumber('min') ?? 0;
		const b = interaction.options.getNumber('max') ?? 10;

		const lower = Math.min(a, b);
		const upper = Math.max(a, b);

		const nerdMode = !Number.isInteger(a) || !Number.isInteger(b);

		const roll = Math.random() * (upper - lower + (nerdMode ? 0 : 1)) + lower;

		const final = nerdMode ? roll : Math.floor(roll);

		interaction.reply(`${nerdMode ? '🤓 ' : ''}${final}`);
	},
};