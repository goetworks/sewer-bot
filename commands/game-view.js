const { SlashCommandBuilder } = require('discord.js');
const sequelize = require('../db/database.js');
const Game = require('../db/models/game.js');
const buildGameEmbed = require('../embeds/game-embed.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('game-view')
		.setDescription('view a registered game')
		.addStringOption(option =>
			option.setName('name')
				.setDescription('name of the game')
				.setRequired(true)),
	async execute(interaction) {
		const gameName = interaction.options.getString('name');
		const game = await Game.findOne({
			where: sequelize.where(sequelize.fn('lower', sequelize.col('name')), sequelize.fn('lower', gameName)),
		});

		if (!game) {
			interaction.reply({ content: `${gameName} does not exist in the database`, ephemeral: true });
			return;
		}

		const embed = buildGameEmbed(interaction.user.username, interaction.user.avatarURL(), game.name, game.description, game.url, game.thumbnail_url, game.start_date);
		interaction.reply({ embeds: [embed] });
	},
};