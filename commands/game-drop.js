const { SlashCommandBuilder } = require('discord.js');
const sequelize = require('../db/database.js');
const Game = require('../db/models/game');

require('dotenv').config();

module.exports = {
	data: new SlashCommandBuilder()
		.setName('game-drop')
		.setDescription('delete a game you registered')
		.addStringOption(option =>
			option.setName('name')
				.setDescription('name of the game you want to drop')
				.setRequired(true)),
	async execute(interaction) {
		const gameName = interaction.options.getString('name');
		const game = await Game.findOne({
			where: sequelize.where(sequelize.fn('lower', sequelize.col('name')), sequelize.fn('lower', gameName)),
		});

		if (!game) {
			interaction.reply({ content: `${gameName} does not exist in the database`, ephemeral: true });
			return;
		}
		if (game.user_id !== interaction.user.id && interaction.user.id !== process.env.ROOT) {
			interaction.reply({ content: `${gameName} is not registered by you`, ephemeral: true });
			return;
		}

		await Game.destroy({ where: { id: game.id } });

		interaction.reply(`${interaction.user.username} just dropped ${game.name}`);
	},
};