const { SlashCommandBuilder } = require('discord.js');
const Game = require('../db/models/game');
const sequelize = require('../db/database.js');
const sanitize = require('sanitize');
const buildDemoEmbed = require('../embeds/demo-embed');
const sanitizer = new sanitize();

module.exports = {
	data: new SlashCommandBuilder()
		.setName('demo-add')
		.setDescription('add a demo to a game you registered')
		.addStringOption(input =>
			input.setName('game-name')
				.setDescription('the name of the game for the demo')
				.setRequired(true))
		.addStringOption(input =>
			input.setName('url')
				.setDescription('url where this demo can be accessed')
				.setRequired(true)),
	async execute(interaction) {
		const gameName = interaction.options.getString('game-name');
		const game = await Game.findOne({
			where: sequelize.where(sequelize.fn('lower', sequelize.col('name')), sequelize.fn('lower', gameName)),
		});

		if (!game) {
			interaction.reply({ content: `${gameName} does not exist in the database`, ephemeral: true });
			return;
		}
		if (game.user_id !== interaction.user.id) {
			interaction.reply({ content: `${gameName} is not registered by you`, ephemeral: true });
			return;
		}

		const protocol = interaction.options.getString('url').includes('https') ? 'https' : 'http';
		const url = sanitizer.value([interaction.options.getString('url'), protocol], 'url');

		try {
			await sequelize.sync();
			const demo = {
				game_id: game.id,
				url: url,
				release_date: new Date().toDateString(),
			};

			await sequelize.models.Demo.create(demo);

			const embed = buildDemoEmbed(interaction.user.username, interaction.user.avatarURL(), `new demo added for ${ game.name}`, game.thumbnail_url, url, null);
			interaction.reply({ embeds: [embed] });
		}
		catch (error) {
			console.error(error);
			interaction.reply({ content: 'something went wrong!!', ephemeral: true });
		}
	},
};