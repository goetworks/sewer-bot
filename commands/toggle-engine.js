const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('toggle-engine-roles')
		.setDescription('toggle engine roles')
		.addStringOption(option =>
			option.setName('role')
				.setDescription('valid strings: godot/unreal/gamemaker/unity')
				.setRequired(true)),
	async execute(interaction) {
		const roleName = interaction.options.getString('role');
		if (!['godot', 'unreal', 'gamemaker', 'unity'].some(r => r == roleName)) {
			interaction.reply({ content: `role "${roleName}" is invalid`, ephemeral: true });
			return;
		}

		const role = interaction.guild.roles.cache.find(r => r.name == roleName);
		const member = interaction.member;
		let message = '';
		if (member.roles.cache.some(r => r.name == role.name)) {
			message = `role "${roleName}" removed`;
			member.roles.remove(role);
		}
		else {
			message = `role "${roleName}" added`;
			member.roles.add(role);
		}

		interaction.reply({ content: message, ephemeral: true });
	},
};