const { SlashCommandBuilder } = require('discord.js');
const Game = require('../db/models/game.js');
const Demo = require('../db/models/demo.js');
const sequelize = require('../db/database.js');
const buildDemoEmbed = require('../embeds/demo-embed.js');
const buildLastThreeDemoEmbed = require('../embeds/demo-last-three.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('demo')
		.setDescription('view the latest builds for games made by people here')
		.addStringOption(input =>
			input.setName('game-name')
				.setDescription('name of a specific game')),
	async execute(interaction) {
		const gameName = interaction.options.getString('game-name');

		// no arg
		if (!gameName) {
			const embed = await buildLastThreeDemoEmbed();
			interaction.reply({ embeds: [embed], ephemeral: true });
			return;
		}

		// yes arg
		const game = await Game.findOne({
			where: sequelize.where(sequelize.fn('lower', sequelize.col('name')), sequelize.fn('lower', gameName)),
		});

		if (!game) {
			interaction.reply({ content: `${gameName} does not exist in the database`, ephemeral: true });
			return;
		}

		const creator = await interaction.guild.members.fetch(game.user_id);
		const demo = await Demo.findOne({
			where: { game_id: game.id },
			order: [['createdAt', 'desc']],
		});
		const embed = buildDemoEmbed(creator.user.username, creator.user.avatarURL(), `latest demo for ${game.name}`, game.thumbnail_url, demo.url, demo.release_date);
		interaction.reply({ embeds: [embed], ephemeral: true });
	},
};