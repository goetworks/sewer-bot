const { SlashCommandBuilder } = require('discord.js');
const buildStartupEmbed = require('../embeds/startup');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('version')
		.setDescription('changelog for the latest release'),
	async execute(interaction) {
		const embed = buildStartupEmbed();
		interaction.reply({ embeds: [embed], ephemeral: true });
	},
};