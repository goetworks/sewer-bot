const { SlashCommandBuilder, TextInputBuilder, TextInputStyle, ModalBuilder, ActionRowBuilder } = require('discord.js');
const sequelize = require('../db/database.js');
const Game = require('../db/models/game.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('game-edit')
		.setDescription('edit a games info')
		.addStringOption(option =>
			option.setName('name')
				.setDescription('name of the game you want to edit, it has to be registered under your user')
				.setRequired(true)),
	async execute(interaction) {
		const gameName = interaction.options.getString('name');
		const game = await Game.findOne({
			where: sequelize.where(sequelize.fn('lower', sequelize.col('name')), sequelize.fn('lower', gameName)),
		});

		if (!game) {
			interaction.reply({ content: `${gameName} does not exist in the database`, ephemeral: true });
			return;
		}
		if (game.user_id !== interaction.user.id) {
			interaction.reply({ content: `${gameName} is not registered by you`, ephemeral: true });
			return;
		}

		const nameInput = new TextInputBuilder()
			.setCustomId('name')
			.setLabel('name')
			.setValue(game.name)
			.setStyle(TextInputStyle.Short);

		const descriptionInput = new TextInputBuilder()
			.setCustomId('description')
			.setLabel('description')
			.setValue(game.description)
			.setStyle(TextInputStyle.Paragraph);

		const urlInput = new TextInputBuilder()
			.setCustomId('url')
			.setLabel('url')
			.setValue(game.url ?? '')
			.setRequired(false)
			.setStyle(TextInputStyle.Short);

		const thumbnailUrl = new TextInputBuilder()
			.setCustomId('thumbnail')
			.setLabel('thumbnail url')
			.setValue(game.thumbnail_url ?? '')
			.setRequired(false)
			.setStyle(TextInputStyle.Short);

		const startDateInput = new TextInputBuilder()
			.setCustomId('startDate')
			.setLabel('development started in')
			.setValue(game.start_date ?? '')
			.setRequired(false)
			.setStyle(TextInputStyle.Short);

		const modal = new ModalBuilder()
			.setCustomId('game edit')
			.setTitle('Register game in development')
			.setComponents(
				new ActionRowBuilder().addComponents(nameInput),
				new ActionRowBuilder().addComponents(descriptionInput),
				new ActionRowBuilder().addComponents(urlInput),
				new ActionRowBuilder().addComponents(thumbnailUrl),
				new ActionRowBuilder().addComponents(startDateInput),
			);

		await interaction.showModal(modal);
	},
};