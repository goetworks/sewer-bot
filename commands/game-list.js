const { EmbedBuilder } = require('@discordjs/builders');
const { SlashCommandBuilder } = require('discord.js');
const Game = require('../db/models/game.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('game-list')
		.setDescription('list some games, either latest added or all games for a specific user')
		.addUserOption(option =>
			option.setName('dev')
				.setDescription('the dev who registered the games')),
	async execute(interaction) {
		const emptyQueryLimit = 10;
		const user = await interaction.options.getUser('dev');

		const games = user ? await Game.findAll({ where: { user_id: user.id }, order: [['createdAt', 'desc']] }) : await Game.findAll({ limit: emptyQueryLimit, order: [['createdAt', 'desc']] });

		const title = user ? `has registered ${games.length} game(s)` : `last ${games.length} game(s) registered: `;

		let message = '';
		for (const game of games) {
			message += `${game.name}`;
			if (!user) {
				const creator = await interaction.guild.members.fetch(game.user_id);
				message += ` (${creator.user.username})`;
			}
			message += '\n';
		}

		const embed = new EmbedBuilder()
			.setAuthor({ name: user ? user.username : interaction.client.user.username, iconURL: user ? user.avatarURL() : interaction.client.user.avatarURL() })
			.setTitle(title)
			.setDescription(message);
		interaction.reply({ embeds: [embed], ephemeral: true });
	},
};