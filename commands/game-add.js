const { ModalBuilder, TextInputBuilder, ActionRowBuilder } = require('@discordjs/builders');
const { SlashCommandBuilder, TextInputStyle } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('game-add')
		.setDescription('register a new game under your name'),
	async execute(interaction) {

		const nameInput = new TextInputBuilder()
			.setCustomId('name')
			.setLabel('name')
			.setStyle(TextInputStyle.Short);

		const descriptionInput = new TextInputBuilder()
			.setCustomId('description')
			.setLabel('description')
			.setStyle(TextInputStyle.Paragraph);

		const urlInput = new TextInputBuilder()
			.setCustomId('url')
			.setLabel('url')
			.setRequired(false)
			.setStyle(TextInputStyle.Short);

		const thumbnailUrl = new TextInputBuilder()
			.setCustomId('thumbnail')
			.setLabel('thumbnail url')
			.setRequired(false)
			.setStyle(TextInputStyle.Short);

		const startDateInput = new TextInputBuilder()
			.setCustomId('startDate')
			.setLabel('development started in')
			.setRequired(false)
			.setStyle(TextInputStyle.Short);

		const modal = new ModalBuilder()
			.setCustomId('game add')
			.setTitle('Register game in development')
			.setComponents(
				new ActionRowBuilder().addComponents(nameInput),
				new ActionRowBuilder().addComponents(descriptionInput),
				new ActionRowBuilder().addComponents(urlInput),
				new ActionRowBuilder().addComponents(thumbnailUrl),
				new ActionRowBuilder().addComponents(startDateInput),
			);

		await interaction.showModal(modal);
	},
};