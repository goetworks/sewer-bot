const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('choose')
		.setDescription('sewerbot shall decide for you if you wish')
		.addStringOption(input =>
			input.setName('options')
				.setDescription('list of options, comma separated')
				.setRequired(true)),
	async execute(interaction) {
		const optionsInput = interaction.options.getString('options');
		const options = optionsInput.split(',');

		if (options.length <= 1 || Math.random() < 0.1) {
			interaction.reply('no');
			return;
		}

		const roll = Math.floor(Math.random() * options.length);
		const choice = options[roll].trim() ? options[roll].trim() : 'nothing';
		interaction.reply(`for you i have chosen\n# ${choice}`);
	},
};