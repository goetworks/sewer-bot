const Game = require('../db/models/game.js');
const Demo = require('../db/models/demo.js');
const { EmbedBuilder } = require('discord.js');

const buildLastThreeDemoEmbed = async () => {
	const demos = await Demo.findAll({
		limit: 3,
		order: [['createdAt', 'desc']],
	});

	const embed = new EmbedBuilder()
		.setTitle('last 3 demos released');

	const fields = [];
	for (const demo of demos) {
		const game = await Game.findOne({ where: { id: demo.game_id } });
		fields.push({ name: `${game.name} (${demo.release_date})`, value: demo.url });
	}

	if (fields.length != 0) {
		embed.addFields(...fields);
	}
	else {
		embed.addFields({ name: 'no demos released... yet', value: 'here is a link that should help you make a demo: https://godotengine.org/' });
	}
	return embed;
};

module.exports = buildLastThreeDemoEmbed;