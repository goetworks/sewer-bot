const fs = require('node:fs');
const path = require('node:path');

const buildStartupEmbed = () => {
	const messagePath = path.join(global.assetPath, '/startup/startup-message.json');
	const json = fs.readFileSync(messagePath, 'utf8');
	const message = JSON.parse(json);

	return {
		color: 0x3EB489,
		title: 'sewer-bot is back online!',
		URL: message.source_url,
		description: `currently running version ${message.version}, latest changes:`,
		fields: message.changelog,
		timestamp: new Date().toISOString(),
	};
};

module.exports = buildStartupEmbed;