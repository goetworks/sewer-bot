const { EmbedBuilder } = require('discord.js');

const buildDemoEmbed = (username, userIcon, title, thumbnailUrl, url, date) => {
	const embed = new EmbedBuilder()
		.setAuthor({ name: username, iconURL: userIcon })
		.setTitle(title)
		.setDescription(url);

	if (thumbnailUrl) embed.setThumbnail(thumbnailUrl);
	if (date) embed.setFooter({ text: `released on ${date}` });

	return embed;
};

module.exports = buildDemoEmbed;