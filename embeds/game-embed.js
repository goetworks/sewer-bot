const { EmbedBuilder } = require('@discordjs/builders');

const buildGameEmbed = (username, userIcon, name, description, url, thumbnail_url, startDate) => {
	const embed = new EmbedBuilder()
		.setAuthor({ name: username, iconURL: userIcon })
		.setTitle(name)
		.setDescription(description);

	if (thumbnail_url) embed.setThumbnail(thumbnail_url);
	if (url) embed.setURL(url);
	if (startDate) embed.setFooter({ text: `development started: ${startDate}` });

	return embed;
};

module.exports = buildGameEmbed;