const { REST, Routes } = require('discord.js');

require('dotenv').config();

const rest = new REST().setToken(process.env.DISCORD_TOKEN);

// dev server specific
rest.put(Routes.applicationGuildCommands(process.env.APPLICATION_ID, process.env.GUILD_ID), { body: [] })
	.then(() => console.log('successfully deleted all guild commands.'))
	.catch(console.error);


// global
rest.put(Routes.applicationCommands(process.env.APPLICATION_ID), { body: [] })
	.then(() => console.log('deleted all commands'))
	.catch(console.error);
