const buildStartupEmbed = require('../embeds/startup');

require('dotenv').config();

module.exports = {
	async report(client) {
		const embed = buildStartupEmbed();
		client.channels.fetch(process.env.GREET_CHANNEL_ID)
			.then(channel => channel.send({ embeds: [embed] }));
	},
};