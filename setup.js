const fs = require('node:fs');
const path = require('node:path');
const { Collection } = require('discord.js');

module.exports = {
	async command(client) {
		client.commands = new Collection();

		const commandsPath = path.join(__dirname, 'commands');
		const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

		for (const file of commandFiles) {
			const filePath = path.join(commandsPath, file);
			const command = require(filePath);
			if ('data' in command && 'execute' in command) {
				client.commands.set(command.data.name, command);
			}
			else {
				console.log(`[WARNING] command at ${filePath} is missing required "data" or "execute" property`);
			}
		}
	},
	events(client) {
		const eventsPath = path.join(__dirname, 'events');
		const eventsFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));

		for (const file of eventsFiles) {
			const filePath = path.join(eventsPath, file);
			const event = require(filePath);
			if (event.once) {
				client.once(event.name, (...args) => event.execute(...args));
			}
			else {
				client.on(event.name, (...args) => event.execute(...args));
			}
		}
	},
	async database() {
		const sequelize = require('./db/database.js');

		// test connection
		try {
			await sequelize.authenticate();
			console.log('database connection established');
		}
		catch (error) {
			console.error('unable to connect to databases:', error);
		}

		// update schemas if neccessary
		// eslint-disable-next-line no-unused-vars
		const Demo = require('./db/models/demo.js');
		// eslint-disable-next-line no-unused-vars
		const Game = require('./db/models/game.js');

		sequelize.sync({ alter: true });
	},
};